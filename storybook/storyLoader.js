// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
	require('./stories/Button/button.stories');
	require('./stories/CenterView/centerView.stories');
	require('./stories/SnapCarousel/snapCarousel.stories');
	require('./stories/Welcome/welcome.stories');
	require('./stories/card/Card.stories');
}

const stories = [
	'./stories/Button/button.stories',
	'./stories/CenterView/centerView.stories',
	'./stories/SnapCarousel/snapCarousel.stories',
	'./stories/Welcome/welcome.stories',
	'./stories/card/Card.stories'
];

module.exports = {
  loadStories,
  stories,
};
