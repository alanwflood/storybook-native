import React from 'react';
import { YellowBox } from 'react-native';
import CenterView from './stories/CenterView';
import {
  getStorybookUI,
  configure,
  addDecorator
} from '@storybook/react-native';
import { loadStories } from './storyLoader';

// Disable yellow warning box for addon-actions events
YellowBox.ignoreWarnings(['Warning: This synthetic event']);

// Add react-native addons for storybook
// https://storybook.js.org/docs/guides/guide-react-native/#on-device-addons
import './rn-addons';

// using decorator to centering all components
// https://storybook.js.org/docs/basics/writing-stories/#using-decorators
addDecorator(getStory => <CenterView>{getStory()}</CenterView>);

// import stories dynamically using react-native-storybook-loader
configure(() => {
  loadStories();
}, module);

// Initialise Storybook
// Refer to
// https://github.com/storybookjs/storybook/tree/master/app/react-native#start-command-parameters
// To find allowed options for getStorybookUI
const StorybookUIRoot = getStorybookUI({});
export default StorybookUIRoot;
