import React from 'react';
import CenterView from './index';
import { Text } from 'react-native';
import { storiesOf } from '@storybook/react-native';

storiesOf('CenterView', module).add('with children', () => (
  <Text>Hello There</Text>
));
