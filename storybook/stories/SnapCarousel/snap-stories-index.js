import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import style from './style';

export default function SnapCarousel({ children }) {
  return <View style={style.main}> {children} </View>;
}

SnapCarousel.defaultProps = {
  children: null
};

SnapCarousel.propTypes = {
  children: PropTypes.node
};
