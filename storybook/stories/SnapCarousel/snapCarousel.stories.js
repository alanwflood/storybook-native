import React from 'react';
import SnapCarousel from '../../../src/snapCarousel/SnapCarousel.js';
// import { Text } from 'react-native';
import {
  storiesOf,
  addParameters,
  addDecorator
} from '@storybook/react-native';
import { withBackgrounds } from '@storybook/addon-ondevice-backgrounds';
import { withKnobs, text } from '@storybook/addon-knobs';

const stories = storiesOf('Car', module);
stories.addDecorator(withKnobs);

addDecorator(withBackgrounds);
addParameters({
  backgrounds: [
    { name: 'dark', value: '#222222' },
    { name: 'white', value: '#ffffff', default: true }
  ]
});

import { action } from '@storybook/addon-actions';

export const carouselItems = [
  {
    title: 'Analytics',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410791/zurich/icon_analytics.png'
  },
  {
    title: 'Design',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_design.png'
  },
  {
    title: 'Technology',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_technology.png'
  },
  {
    title: 'Campaign',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_campaign.png'
  },
  {
    title: 'Strategy',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_strategy.png'
  }
];

storiesOf('SnapCarousel', module)
  .add('default', () => <SnapCarousel size="S" {...carouselItems} />)
  .add('medium', () => <SnapCarousel size="M" {...carouselItems} />)
  .add('large', () => <SnapCarousel size="L" {...carouselItems} />);
