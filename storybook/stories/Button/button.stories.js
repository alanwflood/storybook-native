import React from 'react';
import Button from '../../../src/button/Button';
import { Text } from 'react-native';
import {
  storiesOf,
  addParameters,
  addDecorator
} from '@storybook/react-native';
import { withBackgrounds } from '@storybook/addon-ondevice-backgrounds';
import { actions } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';

const stories = storiesOf('Button', module);
stories.addDecorator(withKnobs);

const eventsFromObject = actions({ onPress: 'you pressed me' });

addDecorator(withBackgrounds);
addParameters({
  backgrounds: [
    { name: 'dark', value: '#222222' },
    { name: 'white', value: '#ffffff', default: true }
  ]
});

storiesOf('Button', module)
  .addParameters({
    component: Button
  })
  .addParameters({
    backgrounds: [
      { name: 'dark', value: '#222222' },
      { name: 'light', value: '#eeeeee', default: true }
    ],
    notes: `
# Custom note\n
This would explain how I work
`
  })
  .addDecorator(withKnobs)
  .add('with text', () => (
    <Button {...eventsFromObject}>
      <Text> {text('Label', 'This is a button')} </Text>
    </Button>
  ))
  .add('with some emoji', () => (
    <Button>
      <Text>{text('Label', '😀 😎 👍 💯')}</Text>
    </Button>
  ));
