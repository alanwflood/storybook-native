import React from 'react';
import Card from '../../../src/card/Card';
import { Text } from 'react-native';
import {
  storiesOf,
  addParameters,
  addDecorator
} from '@storybook/react-native';
import { withBackgrounds } from '@storybook/addon-ondevice-backgrounds';
import { withKnobs, text } from '@storybook/addon-knobs';

const stories = storiesOf('Car', module);
stories.addDecorator(withKnobs);

addDecorator(withBackgrounds);
addParameters({
  backgrounds: [
    { name: 'dark', value: '#222222' },
    { name: 'white', value: '#ffffff', default: true }
  ]
});

storiesOf('Card', module)
  .addParameters({
    component: Card
  })
  .addParameters({
    backgrounds: [
      { name: 'dark', value: '#222222' },
      { name: 'light', value: '#eeeeee', default: true }
    ],
    notes: `
# Custom note\n
This would explain how the card component works
`
  })
  .addDecorator(withKnobs)
  .add('with text', () => (
    <Card>
      <Text>{text('Label', 'This is a button')}</Text>
    </Card>
  ))
  .add('with some emoji', () => (
    <Card>
      <Text>{text('Label', '😀 😎 👍 💯')}</Text>
    </Card>
  ));
