import React from 'react';
import SnapCarousel from '../SnapCarousel';
import renderer from 'react-test-renderer';
import { Text } from 'react-native';

describe('Snap Carousel Component', () => {
  it('Passes a test', () => {
    expect(true).toBe(true);
  });

  it('Does something else', () => {
    expect;
  });
});

const carouselItems = [
  {
    title: 'Analytics',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410791/zurich/icon_analytics.png'
  },
  {
    title: 'Design',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_design.png'
  },
  {
    title: 'Technology',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_technology.png'
  },
  {
    title: 'Campaign',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_campaign.png'
  },
  {
    title: 'Strategy',
    url:
      'https://res.cloudinary.com/bshano/image/upload/v1571410789/zurich/icon_strategy.png'
  }
];

test('renders correctly', () => {
  const tree = renderer
    .create(<SnapCarousel size="m" items={carouselItems} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
