import React from 'react';
import PropTypes from 'prop-types';
import Carousel from 'react-native-snap-carousel';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableHighlight
} from 'react-native';
import ZurichColors from '../styles/colors';

export default class SnapCarousel extends React.Component {
  _renderItem({ item, index }) {
    return (
      <View style={styles.carouselCard}>
        <TouchableHighlight
          onPress={() => {
            alert(`You tapped the ${item.title} button!`);
          }}
          underlayColor={ZurichColors.sosMainHover}
        >
          <Image
            source={{
              uri: item.url
            }}
            style={styles.carouselImage}
          />
        </TouchableHighlight>
        <Text style={styles.carouselText}> {item.title} </Text>
      </View>
    );
  }

  render() {
    return (
      <>
        <View style={styles.carouselContainer}>
          <Text>Snap Carousel size: {this.props.size}</Text>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            layout={'default'} //or tinder, stack
            activeAnimationType={'timing'}
            data={this.props.items}
            sliderWidth={350}
            itemWidth={200}
            renderItem={this._renderItem}
          />
        </View>
      </>
    );
  }
}

SnapCarousel.defaultProps = {
  children: null,
  size: 'md',
  items: []
};

SnapCarousel.propTypes = {
  children: PropTypes.node,
  size: PropTypes.string
  // items: React.PropTypes.arrayOf(
  //   React.PropTypes.shape({
  //     title: React.PropTypes.string,
  //     url: React.PropTypes.string
  //   })
  // )
};

const styles = StyleSheet.create({
  carouselContainer: {
    backgroundColor: ZurichColors.sosSecondary,
    flex: 2.5
  },
  carouselCard: {
    borderColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center'
  },
  carouselImage: {
    width: 170,
    height: 150,
    marginBottom: 20,
    marginTop: 20
  },
  carouselText: {
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  }
});
