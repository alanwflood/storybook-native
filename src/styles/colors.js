const zurichColors = {
  sosMain: '#000066',
  sosMainHover: '#4066b3',
  sosSecondary: '#6ba1d1',
  sosSecondaryHover: '#66c5ec',
  sosStrategy: '#f69c00',
  sosDesign: '#00bfb3',
  sosTechnology: '#ea635c',
  sosAnalytics: '#009ee0',
  sosCampaign: '#ed56b1',
  //
  sosMainTransparent: 'rgba(0, 51, 153, .5)',
  sosLink: '#6ba1d1',
  sosLinkHover: '#000066',
  sosCardMain: '#dce9f4',
  sosCardExpand: '#eaf2f8',
  sosTextSecondary: '#003399'
};

export default zurichColors;
