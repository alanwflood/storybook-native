import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  button: {
    backgroundColor: '#ff6347',
    padding: 25,
    borderRadius: 5
  }
});
export default styles;
