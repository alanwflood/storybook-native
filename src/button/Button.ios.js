import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, Text } from 'react-native';
import styles from './style';

export default function Button({ onPress, children }) {
  return (
    <TouchableHighlight style={styles.button} onPress={onPress}>
      <Text>{children}</Text>
    </TouchableHighlight>
  );
}

Button.defaultProps = {
  children: null,
  onPress: () => {}
};

Button.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func
};
