import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styles from './style';

export default function Card({ children }) {
  return <View style={styles.card}>{children}</View>;
}

Card.defaultProps = {
  children: null
};

Card.propTypes = {
  children: PropTypes.node
};
