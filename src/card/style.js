import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  card: {
    backgroundColor: '#2b2b2b',
    padding: 25,
    borderRadius: 5
  }
});
export default styles;
